import 'package:eceb/screens/auth/facility_login_screen.dart';
import 'package:eceb/screens/auth/individual_login_screen.dart';
import 'package:eceb/screens/home/base.dart';
import 'package:eceb/screens/home/list_of_babies_screen.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case individualLogin:
        return MaterialPageRoute<void>(
          settings: RouteSettings(name: settings.name),
          builder: (_) => IndividualLoginScreen(
            navBarKey: settings.arguments as GlobalKey,
          ),
        );
      case facilityLogin:
        return MaterialPageRoute<void>(
          settings: RouteSettings(name: settings.name),
          builder: (_) => FacilityLoginScreen(
            navBarKey: settings.arguments as GlobalKey,
          ),
        );
      case base:
        return MaterialPageRoute<void>(
          settings: RouteSettings(name: settings.name),
          builder: (_) => Base(
            navBarKey: settings.arguments as GlobalKey,
          ),
        );
      case registerBaby:
        return MaterialPageRoute<void>(
          settings: RouteSettings(name: settings.name),
          builder: (_) => Scaffold(
            body: Center(
              child: Text('Under construction'),
            ),
          ), //TODO: add registerBaby screen
        );
      case babyDetails:
        return MaterialPageRoute<void>(
          settings: RouteSettings(name: settings.name),
          builder: (_) => Scaffold(
            body: Center(
              child: Text('Under construction'),
            ),
          ), //TODO: add babyDetails screen
        );
      case listOfBabies:
        return MaterialPageRoute<void>(
          settings: RouteSettings(name: settings.name),
          builder: (_) => ListofBabiesScreen(),
        );
      default:
        return MaterialPageRoute<void>(
          settings: RouteSettings(name: settings.name),
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route found'),
            ),
          ),
        );
    }
  }
}

const individualLogin = '/individualLogin';
const facilityLogin = '/facilityLogin';
const base = '/base';
const registerBaby = '/registerbaby';
const babyDetails = '/babyDetails';
const listOfBabies = '/listOfBabies';
