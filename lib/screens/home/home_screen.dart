import 'package:eceb/utils/router.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  final GlobalKey navBarKey;

  const HomeScreen({Key? key, required this.navBarKey}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(
        children: [
          Summary(
            navBarKey: navBarKey,
          ),
          SizedBox(
            height: 20,
          ),
          RegisterBabyCard(),
          OnCallDoctors(),
        ],
      ),
    );
  }
}

class SummaryCard extends StatelessWidget {
  final String title;
  final int count;
  final Color iconColor;
  final GlobalKey navBarKey;
  const SummaryCard(
      {Key? key,
      required this.title,
      required this.count,
      required this.iconColor,
      required this.navBarKey})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final navbar = navBarKey.currentWidget;
    return Container(
      width: MediaQuery.of(context).size.width / 4,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: InkWell(
        onTap: () => (navbar as BottomNavigationBar).onTap!(1),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Image.asset(
                  "assets/images/misc/baby_with_diaper.png",
                  color: iconColor,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  title + ": ",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  count.toString(),
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class RegisterBabyCard extends StatelessWidget {
  const RegisterBabyCard({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      color: Theme.of(context).primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 90,
            child: MaterialButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
              onPressed: () {
                Navigator.pushNamed(context, registerBaby);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset(
                        'assets/images/misc/form.png',
                        width: 60,
                      )),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "To Register a Baby",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Summary extends StatelessWidget {
  final GlobalKey navBarKey;

  const Summary({Key? key, required this.navBarKey}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(28, 12, 28, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Summary of 24 hours",
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SummaryCard(
                title: "Admitted",
                count: 14,
                iconColor: Theme.of(context).primaryColor,
                navBarKey: navBarKey,
              ),
              SummaryCard(
                title: "Discharged",
                count: 20,
                iconColor: Colors.grey,
                navBarKey: navBarKey,
              ),
              SummaryCard(
                title: "High risk",
                count: 5,
                iconColor: Colors.red,
                navBarKey: navBarKey,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class OnCallDoctors extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 0, 0, 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text(
              "On Call Doctors",
              style: TextStyle(
                color: Colors.grey,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              DoctorsAvatar(
                name: "Andrea",
                index: 1,
              ),
              DoctorsAvatar(
                name: "Kim",
                index: 2,
              ),
              DoctorsAvatar(
                name: "Jane",
                index: 3,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DoctorsAvatar extends StatelessWidget {
  final int index;
  final String name;
  const DoctorsAvatar({Key? key, required this.index, required this.name})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
              color: Theme.of(context).primaryColor,
              width: 2,
            ),
          ),
          child: CircleAvatar(
            radius: 50,
            backgroundImage: AssetImage(
              'assets/images/doctors/doctor$index.png',
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          name,
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        Text(
          "Online",
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
      ],
    );
  }
}
