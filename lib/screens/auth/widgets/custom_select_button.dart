import 'package:flutter/material.dart';

class CustomSelectButton extends StatelessWidget {
  final String title;
  final Function onPressed;
  const CustomSelectButton({
    required this.title,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: MaterialButton(
        height: 100,
        minWidth: MediaQuery.of(context).size.width * 0.8,
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
          side: BorderSide(
            color: Colors.grey.shade300,
          ),
        ),
        onPressed: () => onPressed(),
        child: Text(
          title,
          style: TextStyle(
            fontSize: 18,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
    );
  }
}
