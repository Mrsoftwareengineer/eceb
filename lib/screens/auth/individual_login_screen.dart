import 'package:eceb/screens/auth/widgets/banners.dart';
import 'package:eceb/screens/auth/widgets/credentials_textfield.dart';
import 'package:eceb/utils/router.dart';
import 'package:flutter/material.dart';

class IndividualLoginScreen extends StatelessWidget {
  final GlobalKey navBarKey;

  const IndividualLoginScreen({Key? key, required this.navBarKey})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: ListView(
        children: [
          Hero(
            tag: "banner",
            child: Material(
              child: Banner2(),
            ),
          ),
          SizedBox(
            height: 40,
          ),
          LoginTextField(hintText: "Local Login Address"),
          LoginTextField(hintText: "Password"),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 100, vertical: 8),
            child: MaterialButton(
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text(
                  "Sign in",
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 16,
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(30),
                ),
                side: BorderSide(
                  color: Colors.grey.shade300,
                ),
              ),
              onPressed: () => Navigator.popAndPushNamed(
                context,
                base,
                arguments: navBarKey,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
